include <threads-scad/threads.scad>
$fn=100;
PLATFORM_LEN=210;
PLATFORM_WIDTH=70;
PLATFORM_THICK=4;

SERVO_HOLE_D=4;
SERVO_THICK=20;
SERVO_LEN=40;
SERVO_WALL_THICK=4;
SERVO_WALL_WIDTH=10;

HOLE_OFFSET_X=5;
HOLE_OFFSET_Z1=5;
HOLE_OFFSET_Z2=15;

DK_THICK=      PLATFORM_THICK;
DK_HOLE_D=     3.2;
DK_HOLE_1_X =  14;
DK_HOLE_1_Y =  7.6;
DK_HOLE_2_X =  66;
DK_HOLE_2_Y =  12.6;
DK_HOLE_3_X =  97;
DK_HOLE_3_Y =  7.6;
DK_HOLE_4_X =  66;
DK_HOLE_4_Y =  40.6;
DK_HOLE_5_X =  15;
DK_HOLE_5_Y =  55.8;

module dk_holes(){
  translate([DK_HOLE_1_X,DK_HOLE_1_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
  translate([DK_HOLE_2_X,DK_HOLE_2_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
  translate([DK_HOLE_3_X,DK_HOLE_3_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
  translate([DK_HOLE_4_X,DK_HOLE_4_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
  translate([DK_HOLE_5_X,DK_HOLE_5_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
}

module servo_walls(){
  difference(){
    cube([SERVO_WALL_WIDTH,SERVO_WALL_THICK,SERVO_THICK]);
    translate([HOLE_OFFSET_X,SERVO_WALL_THICK,HOLE_OFFSET_Z1]) 
    rotate([90,0,0])
    cylinder(d=SERVO_HOLE_D,h=SERVO_WALL_THICK);

    translate([HOLE_OFFSET_X,SERVO_WALL_THICK,HOLE_OFFSET_Z2]) 
    rotate([90,0,0])
    cylinder(d=SERVO_HOLE_D,h=SERVO_WALL_THICK);
  }
}

ROD_EXTRA=1;
ROD_D=10+ROD_EXTRA;
ROD_H=40;

ROD_WALL_THICK=20;
ROD_WALL_DEPTH=ROD_D+4;

ROD_OFFSET_Z=ROD_WALL_DEPTH/2;
ROD_OFFFSET_Y1=PLATFORM_WIDTH* 1/3;
ROD_OFFFSET_Y2=PLATFORM_WIDTH* 2/3;


module rod_wall(){
  translate([0,0,-ROD_WALL_DEPTH])
  ScrewHole(ROD_D, ROD_H,[0,ROD_OFFFSET_Y2,ROD_OFFSET_Z],[0,90,0])
  ScrewHole(ROD_D, ROD_H,[0,ROD_OFFFSET_Y1,ROD_OFFSET_Z],[0,90,0])
  cube([ROD_WALL_THICK,PLATFORM_WIDTH,ROD_WALL_DEPTH]);

}

SERVO_OFFSET=PLATFORM_LEN/2-SERVO_LEN/2;

module plate(){
  cube([PLATFORM_LEN,PLATFORM_WIDTH,PLATFORM_THICK]);
  translate([SERVO_OFFSET-SERVO_WALL_WIDTH,0,PLATFORM_THICK]) servo_walls();
  translate([SERVO_OFFSET+SERVO_LEN,0,PLATFORM_THICK]) servo_walls();


  rod_wall();
  translate([PLATFORM_LEN-ROD_WALL_THICK,0,0]) rod_wall();
}

module plate_with_dk_holes(){
  difference(){
    plate();
    translate([140,5,0])
      dk_holes();
  }
}

module hole_test(){
  difference(){
    translate([0,-25,0])rod_wall();
    translate([0,-100,-100])cube([100,100,100]);
    translate([0,15,-100])cube([100,100,100]);
  }
}




plate_with_dk_holes();
//hole_test();
