$fn=100;
ARM_BOTTOM_WIDTH=9;
ARM_TOP_WIDTH=5;
ARM_TOP_OFFSET=(ARM_BOTTOM_WIDTH-ARM_TOP_WIDTH)/2;
ARM_LEN=14;
CORE_WIDTH=ARM_BOTTOM_WIDTH+1;

module arm(){
  translate([0,-ARM_BOTTOM_WIDTH/2]){
    polygon(points=[[0,0],[0,ARM_BOTTOM_WIDTH],[ARM_LEN,ARM_TOP_OFFSET+ARM_TOP_WIDTH],[ARM_LEN,ARM_TOP_OFFSET]]);

    translate([ARM_LEN,ARM_TOP_OFFSET+ARM_TOP_WIDTH/2]) scale([0.8,1])circle(d=ARM_TOP_WIDTH);
  }
}

module core(){
  square(center=true,[CORE_WIDTH,CORE_WIDTH]);
}


module star_2d(){
  difference(){
    union(){
      core();
      for (i = [0:3]) { 
        rotate([0,0,i*90])translate([CORE_WIDTH/2,0]) arm();
      }
    }
    EDGE_CUT_D=1;
    TMP=4.05;
    for (i = [0:3]) { 
      rotate([0,0,i*90])
        translate([CORE_WIDTH-TMP,CORE_WIDTH-TMP]) circle(r=EDGE_CUT_D);
    }
  }
}

INDENT_DEPTH=3;

module star_3d(){
  linear_extrude(INDENT_DEPTH){
    star_2d();
  }

}

HOLDER_D=CORE_WIDTH+ARM_LEN*2+5;
HOLDER_EXTRA=1;
HOLDER_THICK=INDENT_DEPTH+HOLDER_EXTRA;

module star_holder(){
  difference(){
    cylinder(d=HOLDER_D,HOLDER_THICK);
    translate([0,0,HOLDER_EXTRA])
    star_3d();
  }

}

POLE_L=100;
POLE_D=3;
POLE_THICK=HOLDER_THICK;

module pole(){
  star_holder();
  rotate([0,0,45]) translate([HOLDER_D/2,-POLE_D/2,0]) cube([POLE_L,POLE_D,POLE_THICK]);
}

pole();


