include <threads-scad/threads.scad>

$fn=100;
ROD_D=10;
ROD_H=40;

ROD_PLATE_D=20;
ROD_PLATE_H=4;

THREAD_LEN=2;
PUSHER_WIDTH=(ROD_PLATE_D-ROD_D)/2+THREAD_LEN;
PUSHER_H=4;
PUSHER_THICK=2;

module rod(){
  translate([0,0,ROD_PLATE_H])
  ScrewThread(ROD_D, ROD_H, pitch=0, tooth_angle=30, tolerance=0.4, tip_height=0, tooth_height=0, tip_min_fract=0);
  cylinder(d=ROD_PLATE_D,h=ROD_PLATE_H);
  translate([ROD_D/2-THREAD_LEN,0,ROD_PLATE_H,]) cube([PUSHER_WIDTH,PUSHER_THICK,PUSHER_H]);

}


rod();
