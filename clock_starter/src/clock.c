#include <time.h>
#include <stdio.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/counter.h>
#include <zephyr/drivers/rtc/maxim_ds3231.h>

#include <zephyr/drivers/pwm.h>
#include "servo.h"

#define NOT_TEN 0
#define TEN_AM  1
#define TEN_PM  2


static uint8_t global_is_ten;
static uint8_t global_is_ten;


static const struct device *const ds3231 = DEVICE_DT_GET_ONE(maxim_ds3231);

/* Format times as: YYYY-MM-DD HH:MM:SS DOW DOY */
static const char *format_time(time_t time,
    long nsec)
{
  static char buf[64];
  char *bp = buf;
  char *const bpe = bp + sizeof(buf);
  struct tm tv;
  struct tm *tp = gmtime_r(&time, &tv);

  bp += strftime(bp, bpe - bp, "%Y-%m-%d %H:%M:%S", tp);
  if (nsec >= 0) {
    bp += snprintf(bp, bpe - bp, ".%09lu", nsec);
  }
  bp += strftime(bp, bpe - bp, " %a %j", tp);
  return buf;
}


void check_if_ten(){
  struct tm tv;
  struct tm *tp;
  uint32_t now;

  (void)counter_get_value(ds3231, &now);

  time_t time = now;

  tp = gmtime_r(&time, &tv);
  if (tp->tm_hour != 10 && tp->tm_hour != 22) { 
    global_is_ten = NOT_TEN;
    return;
  }

  if(global_is_ten == NOT_TEN) { // First cycle
    if (tp->tm_hour == 10) { 
      global_is_ten = TEN_AM;
      pendulum_start();
      

    } else if (tp->tm_hour == 22) { 
      global_is_ten = TEN_PM;
      pendulum_stop();
      set_motor_angle(PWM_USEC(1500));
    } 
  }

}

void time_print(){
  uint32_t now = 0;
  (void)counter_get_value(ds3231, &now);
  printk("Now %u: %s\n", now, format_time(now, -1));
}

int time_init(){

  global_is_ten = NOT_TEN;
  if (!device_is_ready(ds3231)) {
    printk("%s: device not ready.\n", ds3231->name);
    return -1;
  }

  uint32_t syncclock_Hz = maxim_ds3231_syncclock_frequency(ds3231);

  printk("DS3231 on %s syncclock %u Hz\n\n", CONFIG_BOARD, syncclock_Hz);

  int rc = maxim_ds3231_stat_update(ds3231, 0, MAXIM_DS3231_REG_STAT_OSF);

  if (rc >= 0) {
    printk("DS3231 has%s experienced an oscillator fault\n",
        (rc & MAXIM_DS3231_REG_STAT_OSF) ? "" : " not");
  } else {
    printk("DS3231 stat fetch failed: %d\n", rc);
    return -1;
  }

  return 0;
}
