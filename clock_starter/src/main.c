/*
 * Copyright (c) 2019-2020 Peter Bigot Consulting, LLC
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>
#include <zephyr/kernel.h>
#include <zephyr/drivers/pwm.h>

#ifdef CONFIG_I2C
#include "clock.h"
#endif

#ifdef CONFIG_PWM
#include "servo.h"
#endif

#ifdef CONFIG_NCS_SAMPLE_MCUMGR_BT_OTA_DFU
#include "bluetooth_smp.h"
#endif



int main(void)
{
#ifdef CONFIG_I2C
  time_init();
  time_print();
#endif

#ifdef CONFIG_PWM
  servo_init();
#endif

#ifdef CONFIG_NCS_SAMPLE_MCUMGR_BT_OTA_DFU
  start_smp_bluetooth();
#endif

  k_sleep(K_MSEC(500));

  while(1){
    k_sleep(K_SECONDS(1));
#ifdef CONFIG_I2C
    check_if_ten();
#endif
  }
  return 0;
}
