
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/pwm.h>

#define SERVO_MOTOR     DT_NODELABEL(servo)
#define PWM_PERIOD   PWM_MSEC(10)

#define SERVO_MOVE   PWM_USEC(4)
#define SERVO_SPEED  K_MSEC(2)

static const struct pwm_dt_spec pwm_servo = PWM_DT_SPEC_GET(DT_NODELABEL(servo));

#define PWM_SERVO_MIN_DUTY_CYCLE    DT_PROP(SERVO_MOTOR, min_pulse)
#define PWM_SERVO_MAX_DUTY_CYCLE    DT_PROP(SERVO_MOTOR, max_pulse)

static int current_duty = PWM_USEC(1000);

int set_motor_angle_fast(uint32_t duty_cycle_ns)
{

  printk("duty_cycle_ns: %d\n",duty_cycle_ns);
  printk("current_duty: %d\n",current_duty);

  int err=0;
  if(current_duty == duty_cycle_ns){
    return 0;
  }

  err = pwm_set_dt(&pwm_servo, PWM_PERIOD, duty_cycle_ns);
  current_duty = duty_cycle_ns;
  return err;
}

int set_motor_angle(uint32_t duty_cycle_ns)
{

  printk("duty_cycle_ns: %d\n",duty_cycle_ns);
  printk("current_duty: %d\n",current_duty);

  int err=0;
  if(current_duty == duty_cycle_ns){
    return 0;
  }

  if(current_duty<duty_cycle_ns){ 
    for(int i = current_duty; i <= duty_cycle_ns; i+=SERVO_MOVE){
      printk("i: %d\n",i);
      k_sleep(SERVO_SPEED);
      err = pwm_set_dt(&pwm_servo, PWM_PERIOD, i);
      if (err) {
        printk("pwm_set_dt_returned %d", err);
      }
      current_duty = i;
    }
  } else {
    for(int i = current_duty; i >= duty_cycle_ns; i-=SERVO_MOVE){
      printk("j: %d\n",i);
      k_sleep(SERVO_SPEED);
      err = pwm_set_dt(&pwm_servo, PWM_PERIOD, i);
      if (err) {
        printk("pwm_set_dt_returned %d", err);
      }
      current_duty = i;
    }
  }
  return err;
}

void pendulum_start(){
#ifdef CONFIG_PWM
  set_motor_angle(PWM_USEC(1000));
  set_motor_angle(PWM_USEC(2000));
  set_motor_angle_fast(PWM_USEC(1000));
#endif
}

void pendulum_stop(){
#ifdef CONFIG_PWM
  set_motor_angle(PWM_USEC(1600));
  k_sleep(K_SECONDS(1));
  set_motor_angle(PWM_USEC(1000));
#endif
}

void servo_init(){
  current_duty = PWM_USEC(1000);

  if (!pwm_is_ready_dt(&pwm_servo)) {
    printk("Error: PWM device is not ready\n");
    return;
  }

}
