$fn=100;

DK_LEN=     99;
DK_WIDTH=   64.5;
DK_THICK=   3;

CORNER_RADIUS=3;

ANTENNA_OFFSET_Y=7.5;
ANTENNA_LEN=38;
ANTENNA_WIDTH=2.5;
ANTENNA_ANGLE=45;

DK_HOLE_D=     3.2;

DK_HOLE_1_X =  14;
DK_HOLE_1_Y =  7.6;

DK_HOLE_2_X =  66;
DK_HOLE_2_Y =  12.6;

DK_HOLE_3_X =  97;
DK_HOLE_3_Y =  7.6;

DK_HOLE_4_X =  66;
DK_HOLE_4_Y =  40.6;

DK_HOLE_5_X =  15;
DK_HOLE_5_Y =  55.8;

module dk_holes(){
  translate([DK_HOLE_1_X,DK_HOLE_1_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
  translate([DK_HOLE_2_X,DK_HOLE_2_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
  translate([DK_HOLE_3_X,DK_HOLE_3_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
  translate([DK_HOLE_4_X,DK_HOLE_4_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
  translate([DK_HOLE_5_X,DK_HOLE_5_Y,0])
    cylinder(h=DK_THICK,d=DK_HOLE_D);
}


module plate() {
  linear_extrude(height=DK_THICK) {
    translate([CORNER_RADIUS,CORNER_RADIUS])
      minkowski() {
        square([DK_LEN - CORNER_RADIUS*2, DK_WIDTH - CORNER_RADIUS*2]);
        circle(r=CORNER_RADIUS);
      }
      translate([DK_LEN,ANTENNA_OFFSET_Y])
      difference(){
        square([ANTENNA_WIDTH,ANTENNA_LEN]);
          rotate(-ANTENNA_ANGLE)
            square([ANTENNA_WIDTH*2,ANTENNA_WIDTH*2]);
        translate([0,ANTENNA_LEN])
          rotate(-ANTENNA_ANGLE)
            square([ANTENNA_WIDTH*2,ANTENNA_WIDTH*2]);
      }
  }
}

module DK(){
  difference(){
    plate();
    dk_holes();
  }
}

DK();
