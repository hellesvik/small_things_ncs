/*
 * Copyright (c) 2019 Peter Bigot Consulting, LLC
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/ {
    servo0: servo0 {
        compatible = "pwm-servo";
        pwms = <&pwm0 0 PWM_MSEC(20) PWM_POLARITY_NORMAL>;
        min-pulse = <PWM_USEC(1000)>;
        max-pulse = <PWM_USEC(2000)>;
    };
    servo1: servo1 {
        compatible = "pwm-servo";
        pwms = <&pwm1 0 PWM_MSEC(20) PWM_POLARITY_NORMAL>;
        min-pulse = <PWM_USEC(1000)>;
        max-pulse = <PWM_USEC(2000)>;
    };
};

&pwm0 {
    status = "okay";
    pinctrl-0 = <&pwm0_custom_motor>;
    pinctrl-1 = <&pwm0_csleep_motor>;
    pinctrl-names = "default", "sleep";
};

&pwm1 {
    status = "okay";
    pinctrl-0 = <&pwm1_custom_motor>;
    pinctrl-1 = <&pwm1_csleep_motor>;
    pinctrl-names = "default", "sleep";
};

&pinctrl {
    pwm0_custom_motor: pwm0_custom_motor {
        group1 {
            psels = <NRF_PSEL(PWM_OUT0, 1, 14)>;
        };
    };

    pwm0_csleep_motor: pwm0_csleep_motor {
        group1 {
            psels = <NRF_PSEL(PWM_OUT0, 1, 14)>;
            low-power-enable;
        };
    };
    pwm1_custom_motor: pwm1_custom_motor {
        group1 {
            psels = <NRF_PSEL(PWM_OUT0, 1, 13)>;
        };
    };

    pwm1_csleep_motor: pwm1_csleep_motor {
        group1 {
            psels = <NRF_PSEL(PWM_OUT0, 1, 13)>;
            low-power-enable;
        };
    };
};
