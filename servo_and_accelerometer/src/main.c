/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>

#include "servo.h"
#include <stdlib.h>

int main(void)
{
  servo_init();
	printf("Hello World! %s\n", CONFIG_BOARD_TARGET);

  unsigned int seed = k_uptime_get_32();
  srand(seed);

  int X = 100; // Replace with your desired minimum value
  int Y = 2000; // Replace with your desired maximum value

  int random_num = 1;

  while(1){

    random_num = (rand() % (Y - X + 1)) + X;
    walk_forward();
    k_sleep(K_MSEC(random_num));
    random_num = (rand() % (Y - X + 1)) + X;
    walk_backward();
    k_sleep(K_MSEC(random_num));
  }

	return 0;
}
