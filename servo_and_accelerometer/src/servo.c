
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/pwm.h>

#define SERVO_MOTOR_0     DT_NODELABEL(servo0)
#define SERVO_MOTOR_1     DT_NODELABEL(servo1)
#define PWM_PERIOD   PWM_MSEC(10)

#define SERVO_MOVE   PWM_USEC(4)
#define SERVO_SPEED  K_MSEC(8)

static const struct pwm_dt_spec pwm_servo0 = PWM_DT_SPEC_GET(DT_NODELABEL(servo0));
static const struct pwm_dt_spec pwm_servo1 = PWM_DT_SPEC_GET(DT_NODELABEL(servo1));

#define PWM_SERVO_MIN_DUTY_CYCLE    DT_PROP(SERVO_MOTOR, min_pulse)
#define PWM_SERVO_MAX_DUTY_CYCLE    DT_PROP(SERVO_MOTOR, max_pulse)

static int current_duty = PWM_USEC(1000);

int set_motor_angle_fast(uint32_t duty_cycle_ns)
{

  printk("duty_cycle_ns: %d\n",duty_cycle_ns);
  printk("current_duty: %d\n",current_duty);

  int err=0;
  if(current_duty == duty_cycle_ns){
    return 0;
  }

  err = pwm_set_dt(&pwm_servo0, PWM_PERIOD, duty_cycle_ns);
  err = pwm_set_dt(&pwm_servo1, PWM_PERIOD, duty_cycle_ns);
  current_duty = duty_cycle_ns;
  return err;
}

int set_motor_angle(uint32_t duty_cycle_ns)
{

  printk("duty_cycle_ns: %d\n",duty_cycle_ns);
  printk("current_duty: %d\n",current_duty);

  int err=0;
  if(current_duty == duty_cycle_ns){
    return 0;
  }

  if(current_duty<duty_cycle_ns){ 
    for(int i = current_duty; i <= duty_cycle_ns; i+=SERVO_MOVE){
      //printk("i: %d\n",i);
      k_sleep(SERVO_SPEED);
      err = pwm_set_dt(&pwm_servo0, PWM_PERIOD, i);
      err = pwm_set_dt(&pwm_servo1, PWM_PERIOD, i);
      if (err) {
        printk("pwm_set_dt_returned %d", err);
      }
      current_duty = i;
    }
  } else {
    for(int i = current_duty; i >= duty_cycle_ns; i-=SERVO_MOVE){
      //printk("j: %d\n",i);
      k_sleep(SERVO_SPEED);
      err = pwm_set_dt(&pwm_servo0, PWM_PERIOD, i);
      err = pwm_set_dt(&pwm_servo1, PWM_PERIOD, i);
      if (err) {
        printk("pwm_set_dt_returned %d", err);
      }
      current_duty = i;
    }
  }
  return err;
}


void walk_forward(){
#ifdef CONFIG_PWM
  set_motor_angle(PWM_USEC(2100));
#endif
}

void walk_backward(){
#ifdef CONFIG_PWM
  set_motor_angle(PWM_USEC(1800));
#endif
}


void servo_init(){
  current_duty = PWM_USEC(2000);

  if (!pwm_is_ready_dt(&pwm_servo0)) {
    printk("Error: PWM0 device is not ready\n");
    return;
  }

  if (!pwm_is_ready_dt(&pwm_servo1)) {
    printk("Error: PWM1 device is not ready\n");
    return;
  }

}
