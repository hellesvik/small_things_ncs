#ifndef __SERVO_H__
#define __SERVO_H__

int set_motor_angle(uint32_t duty_cycle_ns);
int set_motor_angle_fast(uint32_t duty_cycle_ns);
void walk_forward();
void walk_backward();
void servo_init();

#endif // __SERVO_H__
