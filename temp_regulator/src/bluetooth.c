#include <zephyr/kernel.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>

#include "bluetooth.h"
#include "temp_sensor.h"
#include "heat_element.h"
#include "control.h"

static uint8_t power;
static bool notif_enabled = false;

#define POWER_ON  11
#define POWER_OFF 12

static ssize_t toast_power(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf,
			 uint16_t len, uint16_t offset, uint8_t flags)
{
  power = *((uint8_t *)buf);

  if(power == POWER_ON){
    printk("Power on\n");
    control_start();
  } else if(power == POWER_OFF){
    printk("Power off\n");
    control_stop();
  } else {
    printk("Power command not recognized\n");
  }

  return 0;
}

static void target_temp_cfg_changed_cb(const struct bt_gatt_attr *attr,
				  uint16_t value)
{

	notif_enabled = (value == BT_GATT_CCC_NOTIFY);

	printk("HRS notifications %s\n", notif_enabled ? "enabled" : "disabled");
}

static void temp_cfg_changed_cb(const struct bt_gatt_attr *attr,
				  uint16_t value)
{

	notif_enabled = (value == BT_GATT_CCC_NOTIFY);

	printk("HRS notifications %s\n", notif_enabled ? "enabled" : "disabled");
}

BT_GATT_SERVICE_DEFINE(toast_svc, BT_GATT_PRIMARY_SERVICE(BT_UUID_TOAST),

    BT_GATT_CHARACTERISTIC(BT_UUID_TOAST_TEMPERATURE,
              BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_NONE,
              NULL, NULL, NULL),

    BT_GATT_CCC(temp_cfg_changed_cb,
        BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),

    BT_GATT_CHARACTERISTIC(BT_UUID_TOAST_TARGET_TEMP,
              BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_NONE,
              NULL, NULL, NULL),

    BT_GATT_CCC(target_temp_cfg_changed_cb,
        BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),

    BT_GATT_CHARACTERISTIC(BT_UUID_TOAST_POWER,
              BT_GATT_CHRC_WRITE_WITHOUT_RESP,
              BT_GATT_PERM_WRITE, NULL, toast_power, 
              NULL),

);

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
};

static const struct bt_data sd[] = {
	BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_VAL_TOAST),
};


static void connected(struct bt_conn *conn, uint8_t err)
{
	if (err) {
		printk("Connection failed (err %u)\n", err);
		return;
	}
	printk("Connected\n");
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
	printk("Disconnected (reason %u)\n", reason);
}

BT_CONN_CB_DEFINE(conn_callbacks) = {
	.connected        = connected,
	.disconnected     = disconnected,
};

int bt_toast_temp_notify(int16_t temperature)
{
  if(!notif_enabled) {
    printk("Not enabled yet\n");
    return -1;
  }

	int rc;

	static uint8_t pkg[2];

	pkg[0] = 0x06; /* uint8, sensor contact */
	pkg[1] = temperature;
	rc = bt_gatt_notify(NULL, &toast_svc.attrs[1], &pkg, sizeof(pkg));
	return rc == -ENOTCONN ? 0 : rc;
}

int bt_toast_target_temp_notify(int16_t target_temp)
{
  if(!notif_enabled) {
    printk("Not enabled yet\n");
    return -1;
  }

	int rc;


	rc = bt_gatt_notify(NULL, &toast_svc.attrs[4], &target_temp, sizeof(target_temp));
	return rc == -ENOTCONN ? 0 : rc;
}

void bluetooth_init(){
  int err;

	err = bt_enable(NULL);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}
	err = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad),
			      sd, ARRAY_SIZE(sd));
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}
	printk("Advertising successfully started\n");
}
