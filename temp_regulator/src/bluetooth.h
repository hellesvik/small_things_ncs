#ifndef __BLUETOOTH_INIT_H__
#define __BLUETOOTH_INIT_H__

#define DEVICE_NAME             CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN         (sizeof(DEVICE_NAME) - 1)

#define BT_UUID_VAL_TOAST \
	BT_UUID_128_ENCODE(0x00001523, 0x1212, 0x8eee, 0x1523, 0x70a5770a5700)

#define BT_UUID_VAL_TOAST_POWER \
	BT_UUID_128_ENCODE(0x00001524, 0x1212, 0x8eee, 0x1523, 0x70a5770a5700)

#define BT_UUID_VAL_TOAST_TEMPERATURE \
	BT_UUID_128_ENCODE(0x00001525, 0x1212, 0x8eee, 0x1523, 0x70a5770a5700)

#define BT_UUID_VAL_TOAST_HOLD_TIME \
	BT_UUID_128_ENCODE(0x00001526, 0x1212, 0x8eee, 0x1523, 0x70a5770a5700)

#define BT_UUID_VAL_TOAST_TARGET_TEMP \
	BT_UUID_128_ENCODE(0x00001527, 0x1212, 0x8eee, 0x1523, 0x70a5770a5700)

#define BT_UUID_TOAST BT_UUID_DECLARE_128(BT_UUID_VAL_TOAST)
#define BT_UUID_TOAST_POWER BT_UUID_DECLARE_128(BT_UUID_VAL_TOAST_POWER)
#define BT_UUID_TOAST_TEMPERATURE BT_UUID_DECLARE_128(BT_UUID_VAL_TOAST_TEMPERATURE)
#define BT_UUID_TOAST_HOLD_TIME BT_UUID_DECLARE_128(BT_UUID_VAL_TOAST_TEMPERATURE)
#define BT_UUID_TOAST_TARGET_TEMP BT_UUID_DECLARE_128(BT_UUID_VAL_TOAST_TARGET_TEMP)

/** @brief Callback type for turning toasting iron on or off. */
typedef void (*power_cb_t)(const bool power_state);

/** @brief Callback type for when the temperature is pulled. */
typedef bool (*temp_cb_t)(const int temperature);

int bt_toast_temp_notify(int16_t temperature);
int bt_toast_target_temp_notify(int16_t target_temp);
void bluetooth_init();
#endif // __BLUETOOTH_INIT_H__
