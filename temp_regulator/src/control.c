#include <stdio.h>
#include <stdlib.h>
#include <zephyr/kernel.h>

#include "control.h"
#include "PID.h"
#include "temp_sensor.h"
#include "heat_element.h"
#include "bluetooth.h"

struct temp_time sequence[] = { 
  {80,30}, 
  {120,120}, 
  {170,30},
  {60,180}
};

#define PERIOD 1000
#define TEMP_TOLERANCE 3

#define PID_KP  4.0f
#define PID_KI  0.008f
#define PID_KD  0.0f

#define PID_TAU (float) (PERIOD/1000)

// Min 0 since we only have heat output
// If we want negative, we need to add a fan
#define PID_LIM_MIN 0.0f
#define PID_LIM_MAX (float)PERIOD

#define PID_LIM_PART (2/10)
#define PID_LIM_MIN_INT (float)  -200
#define PID_LIM_MAX_INT (float)   200

//TODO: FIgure out what this is
#define SAMPLE_TIME_S 1.0f

#define SAFETY_MAX_RUNTIME 1999
#define SAFETY_MAX_TEMP 200
#define SAFETY_MIN_TEMP 0
#define SAFETY_TEMP_CHANGE_PER_SEC 8
#define SAFETY_TEMP_SAME_MAX_TIMES 10

static uint8_t control_running;

static float temperature = 20.0f;

static PIDController pid = { PID_KP, PID_KI, PID_KD,
  PID_TAU,
  PID_LIM_MIN, PID_LIM_MAX,
  PID_LIM_MIN_INT, PID_LIM_MAX_INT,
  SAMPLE_TIME_S };

void control_init(){
  /* Initialise PID controller */
  PIDController_Init(&pid);
  control_running = 0;
}

static float prev_temperature = -1;
static float same_temperature_counter = 0;

static int safety_check(int runtime){

  if(runtime > SAFETY_MAX_RUNTIME){
    printk("Error: Ran for too long\n");
    return 1;
  }
  if( temperature > SAFETY_MAX_TEMP){
    printk("Error: Too high temperature!\n");
    return 1;
  }
  if( temperature < SAFETY_MIN_TEMP){
    printk("Error: Too low temperature!\n");
    return 1;
  }
  if(prev_temperature == -1){
    prev_temperature = temperature;
  }
  if(temperature - prev_temperature > SAFETY_TEMP_CHANGE_PER_SEC){
    printk("Error: Temperature rises too fast!\n");
    return 1;
  }
  if(temperature == prev_temperature){
    same_temperature_counter++;
  }
  else {
    same_temperature_counter = 0;
  }
  if(same_temperature_counter >= SAFETY_TEMP_SAME_MAX_TIMES){
    printk("Error: Temperature have not changed for too long!\n");
    return 1;
  }


  prev_temperature = temperature;

  return 0;
}

void control_run(struct temp_time *points, size_t len){
  uint16_t time_on;
  uint16_t time_off;

  uint32_t i =0;
  uint32_t hold_time =0;

  float target;
  bool on_target = false;

  int runtime = 0;

  while(1) {
    printk("-----------------------\n");
    printk("Total time: %d, Current hold: %d\n",runtime++,hold_time);
    if (i >= len){
      printk("Loop done\n");
      break;
    }
    target = points[i].temp_c;
    bt_toast_target_temp_notify(target);

    temperature = get_temp();
    PIDController_Update(&pid, target, temperature);
    time_off = PERIOD - (uint16_t)pid.out; 
    time_on = PERIOD - time_off;

    printf("Meas: %.2f\t Target: %.2f\n", temperature, target);
    printf("Time_on: %d\tTime_off: %d\n",time_on,time_off);

    if( safety_check(runtime)){
      break;
    }

    bt_toast_temp_notify(temperature);

    heat_set(HEAT_ON);
    k_sleep(K_MSEC(time_on));
    heat_set(HEAT_OFF);
    k_sleep(K_MSEC(time_off));

    if( target - TEMP_TOLERANCE <= temperature && temperature <= target + TEMP_TOLERANCE) {
      on_target = true;
    }

    if(on_target) {
      hold_time++;
      if(points[i].time_sec <=hold_time){
        i++;
        control_init();
        on_target = false;
        hold_time = 0;
      }
    }
  }
  heat_set(HEAT_OFF);
}

#define CONTROL_STACK_SIZE 40000
#define CONTROL_PRIORITY 7

K_THREAD_STACK_DEFINE(control_thread_stack_area, CONTROL_STACK_SIZE);
struct k_thread control_thread;
k_tid_t control_thread_id;


void control_thread_fn(void *p1, void *p2, void *p3){

  int length = sizeof(sequence) / sizeof(sequence[0]);
  control_run(sequence,length);

}



void control_start(){

  if(control_running) {
    return;
  }
  control_running = 1;
  same_temperature_counter = 0;
  control_thread_id = k_thread_create(&control_thread, control_thread_stack_area,
      K_THREAD_STACK_SIZEOF(control_thread_stack_area),
      control_thread_fn, NULL, NULL, NULL,
      CONTROL_PRIORITY, 0, K_NO_WAIT);
  k_thread_start(control_thread_id);
}

void control_stop(){
  if(!control_running){
    return;
  }
  control_running = 0;
  k_thread_abort(control_thread_id);
  k_sleep(K_MSEC(100));
  bt_toast_target_temp_notify(0);
  heat_set(HEAT_OFF);
}
