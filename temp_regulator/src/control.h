#ifndef __CONTROL_H__
#define __CONTROL_H__

struct temp_time {
  uint16_t temp_c;
  uint16_t time_sec;
};

void control_init();
void control_run(struct temp_time *points, size_t len);
void control_start();
void control_stop();

#endif // __CONTROL_H__
