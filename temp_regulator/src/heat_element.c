#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>

#include "heat_element.h"

#define HEAT_RELAY_DT DT_ALIAS(led0)
static const struct gpio_dt_spec heat_relay = GPIO_DT_SPEC_GET(HEAT_RELAY_DT, gpios);

void heat_init(){
  int ret;
	if (!gpio_is_ready_dt(&heat_relay)) {
		return;
	}
	ret = gpio_pin_configure_dt(&heat_relay, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return;
	}
		ret = gpio_pin_set_dt(&heat_relay,HEAT_OFF);
		if (ret < 0) {
			return;
		}
}

void heat_set(bool on){
  int ret;
		ret = gpio_pin_set_dt(&heat_relay,on);
		if (ret < 0) {
			return;
		}
}
