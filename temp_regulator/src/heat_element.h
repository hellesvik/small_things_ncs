#ifndef __HEAT_ELEMENT_H__
#define __HEAT_ELEMENT_H__

#define HEAT_ON   1
#define HEAT_OFF  0

void heat_init();
void heat_set(bool on);
#endif //__HEAT_ELEMENT_H__
