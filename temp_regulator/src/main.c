#include <zephyr/kernel.h>

#include "temp_sensor.h"
#include "bluetooth.h"
#include "heat_element.h"
#include "control.h"

int main(void)
{
  heat_init();
  bluetooth_init();
  control_init();

  //BLE will be able to start threads in control

  printk("Welcome to Toasting!\n");
  while(1){

    int temperature = get_temp();
    printk("temp: %d\n",temperature);
    k_sleep(K_SECONDS(1));
  }
}
